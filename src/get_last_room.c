/*
** EPITECH PROJECT, 2018
** $NAME_OF_THE_PROJECT
** File description:
** Here is coded a super function
*/

#include "lemin.h"

room *get_last_room(room *list)
{
    while (list->next != NULL) {
        list = list->next;
    }
    return (list);
}
