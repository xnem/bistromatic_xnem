/*
** EPITECH PROJECT, 2018
** $NAME_OF_THE_PROJECT
** File description:
** Here is coded a super function
*/

#include "lemin.h"

void add_last_node_linked(room **list_addr, room_linked *node)
{
    room *list = *list_addr;
    room_linked *tmp = list->linked_rooms;

    if (list->linked_rooms == NULL) {
        list->linked_rooms = node;
        return ;
    }
    while (tmp->next != NULL) {
        tmp = tmp->next;
    }
    tmp->next = node;
}
