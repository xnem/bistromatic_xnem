/*
** EPITECH PROJECT, 2018
** $NAME_OF_THE_PROJECT
** File description:
** Here is coded a super function
*/

#include "lemin.h"

int tree_course(ant ant_t, room *predecessor, room_linked *list) 
{
    if (list == NULL)
        return (0);
    while (list != NULL) {
        if ((predecessor->length + 1 < list->adress->length
                    || list->adress->visited == 0)
                && room_is_usable(ant_t, list->adress)) {
            list->adress->predecessor = predecessor;
            list->adress->length = predecessor->length + 1;
            list->adress->visited = 1;
            tree_course(ant_t, list->adress, list->adress->linked_rooms);
        }
        list = list->next;
    }
}
