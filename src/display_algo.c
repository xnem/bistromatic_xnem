/*
** EPITECH PROJECT, 2018
** $NAME_OF_THE_PROJECT
** File description:
** Here is coded a super function
*/

#include "lemin.h"

void display_algo(ant ant_t, room *room_t)
{
    my_putstr("P");
    my_put_nbr(ant_t.name);
    my_putstr("-");
    my_putstr(room_t->name);
}
