/*
** EPITECH PROJECT, 2018
** $NAME_OF_THE_PROJECT
** File description:
** Here is coded a super function
*/

#include "lemin.h"

room *create_node(char *str)
{
    room *new_node = NULL;

    new_node = malloc(sizeof(room));
    if (new_node == NULL)
        exit (84);
    new_node->name = my_strdup(get_string(&str));
    my_putstr(new_node->name);
    my_putstr(" ");
    new_node->x = my_getnbr(get_string(&str));
    my_put_nbr(new_node->x);
    my_putstr(" ");
    new_node->y = my_getnbr(get_string(&str));
    my_put_nbr(new_node->y);
    my_putstr("\n");
    new_node->next = NULL;
    new_node->linked_rooms = NULL;
    return (new_node);
}
