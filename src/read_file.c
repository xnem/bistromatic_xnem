/*
** EPITECH PROJECT, 2018
** $NAME_OF_THE_PROJECT
** File description:
** Here is coded a super function
*/

#include "lemin.h"

void create_rooms(char *line, int nb_ants, room **last, room **list)
{
    size_t size = 0;

    if (!my_strcmp(line, "##start\n")) {
        getline(&line, &size, stdin);
        create_first_node(line, nb_ants, list);
        return ;
    }
    if (!my_strcmp(line, "##end\n")) {
        my_putstr(line);
        getline(&line, &size, stdin);
        *last = create_node(line);
        return ;
    }
    if (line[0] != '#') {
        add_last_node(list, create_node(line));
        return ;
    }
}

room *read_file(void)
{
    char *line = NULL;
    size_t size = 0;
    int nb_ants;
    room *list = create_empty_node();
    room *last;

    my_putstr("#number_of_ants\n");
    getline(&line, &size, stdin);
    nb_ants = my_getnbr(line);
    my_putstr(line);
    my_putstr("#rooms\n");
    while (getline(&line, &size, stdin) != -1 && !check_letter(line, '-'))
        create_rooms(line, nb_ants, &last, &list);
    add_last_node(&list, last);
    get_tunnels(&list, line);
    my_putstr("#moves\n");
    return (list);
}
