/*
** EPITECH PROJECT, 2018
** $NAME_OF_THE_PROJECT
** File description:
** Here is coded a super function
*/

#include "lemin.h"

int room_is_usable(ant ant_t, room *room_t)
{
    room *last;

    if (room_t == NULL)
        return (0);
    last = get_last_room(room_t);
    if (is_in_last_room(ant_t, last))
        return (0);
    if (room_t->name == last->name)
        return (1);
    return (!(room_t->nb_ant));
}
