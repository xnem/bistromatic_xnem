/*
** EPITECH PROJECT, 2018
** $NAME_OF_THE_PROJECT
** File description:
** Here is coded a super function
*/

#include "lemin.h"

ant *init_ants(room *list)
{
    int nb = list->nb_ant;
    ant *tab_ants = NULL;

    tab_ants = malloc(sizeof(ant) * (nb + 1));
    if (tab_ants == NULL)
        exit(84);
    for (int i = 0; i < nb; i++) {
        tab_ants[i].name = i + 1;
        tab_ants[i].room_in = list;
    }
    return (tab_ants);
}
