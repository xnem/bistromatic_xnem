/*
** EPITECH PROJECT, 2018
** $NAME_OF_THE_PROJECT
** File description:
** Here is coded a super function
*/

#include "lemin.h"

room_linked *room_dup(room *node)
{
    room_linked *new_room = NULL;

    new_room = malloc(sizeof(room_linked));
    if (new_room == NULL)
        exit(84);
    new_room->name = node->name;
    new_room->adress = node;
    return (new_room);
}
