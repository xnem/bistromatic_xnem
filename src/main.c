/*
** EPITECH PROJECT, 2018
** $NAME_OF_THE_PROJECT
** File description:
** Here is coded a super function
*/

#include "lemin.h"

int main(int ac, char **av)
{
    room *list_rooms = read_file();
    ant *tab_ants = init_ants(list_rooms);
    room *last = get_last_room(list_rooms);

    //algo(list_rooms, tab_ants, last);
    init_map(list_rooms);
    tree_course(tab_ants[0], list_rooms, list_rooms->linked_rooms);
    get_best_room(tab_ants[0], list_rooms);
    move_ant(1, "2", &list_rooms, &tab_ants);
    display_list(list_rooms);
    init_map(list_rooms);
    tree_course(tab_ants[1], list_rooms, list_rooms->linked_rooms);
    display_list(list_rooms);
    get_best_room(tab_ants[1], list_rooms);
    return (0);
}
