/*
** EPITECH PROJECT, 2018
** $NAME_OF_THE_PROJECT
** File description:
** Here is coded a super function
*/

#include "lemin.h"

char *get_string_dash(char **str1)
{
    int size;
    char *string = NULL;
    char *str = *str1;

    while (*str == ' ')
        str++;
    for (size = 0; str[size] && !(str[size] == '-' || str[size] == '\n');
            size++);
    string = malloc(sizeof(char) * (size + 1));
    if (string == NULL)
        exit(84);
    for (size = 0; str[size] && !(str[size] == '-' || str[size] == '\n');
            size++)
        string[size] = str[size];
    string[size] = '\0';
    str += size;
    if (*str == '-')
        str++;
    *str1 = str;
    return (string);
}
