/*
** EPITECH PROJECT, 2018
** $NAME_OF_THE_PROJECT
** File description:
** Here is coded a super function
*/

#include "lemin.h"

int display_error(char *str_error, char *str)
{
    int len;
    int len2;
    char *str_err = NULL;

    len = my_strlen(str_error);
    len2 = my_strlen(str);
    str_err = malloc(sizeof(char) * (len + len2 + 4));
    if (str_err == NULL)
        exit (84);
    str_err[0] = '\0';
    str_err = my_strcat(str_err, str);
    str_err = my_strcat(str_err, ": ");
    str_err = my_strcat(str_err, str_error);
    str_err = my_strcat(str_err, "\n");
    str_err[len + len2 + 4] = '\0';
    write(2, str_err, len + len2 + 4);
    free(str_err);
    exit (84);
}
