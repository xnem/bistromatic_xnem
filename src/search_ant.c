/*
** EPITECH PROJECT, 2018
** $NAME_OF_THE_PROJECT
** File description:
** Here is coded a super function
*/

#include "lemin.h"

int search_ant(int ant_t, ant *tab_ants)
{
    int i;

    for (i = 0; tab_ants[i].room_in; i++) {
        if (tab_ants[i].name == ant_t)
            return (i);
    }
    return (-1);
}
