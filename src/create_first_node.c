/*
** EPITECH PROJECT, 2018
** $NAME_OF_THE_PROJECT
** File description:
** Here is coded a super function
*/

#include "lemin.h"

void *create_first_node(char *str, int nb_ant, room **list)
{
    my_putstr("##start\n");
    (*list)->name = my_strdup(get_string(&str));
    my_putstr((*list)->name);
    my_putstr(" ");
    (*list)->x = my_getnbr(get_string(&str));
    my_put_nbr((*list)->x);
    my_putstr(" ");
    (*list)->y = my_getnbr(get_string(&str));
    my_put_nbr((*list)->y);
    my_putstr("\n");
    (*list)->nb_ant = nb_ant;
}
