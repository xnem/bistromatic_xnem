/*
** EPITECH PROJECT, 2018
** $NAME_OF_THE_PROJECT
** File description:
** Here is coded a super function
*/

#include "lemin.h"

void algo(room *list_rooms, ant *tab_ant, room *last)
{
    room *tmp;
    int nb_ants = list_rooms->nb_ant;
    int first_space;

    while (last->nb_ant != nb_ants) {
        first_space = 0;
        for (int i = 0; tab_ant[i].room_in != NULL; i++) {
            tmp = get_accessible_rooms(tab_ant[i]);
            if (room_is_usable(tab_ant[i], tmp)) {
                move_ant(tab_ant[i].name, tmp->name, &list_rooms, &tab_ant);
                if (first_space)
                    my_putstr(" ");
                display_algo(tab_ant[i], tmp);
                first_space = 1;
            }
        }
        my_putstr("\n");
    }
}
