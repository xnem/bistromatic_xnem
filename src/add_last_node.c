/*
** EPITECH PROJECT, 2018
** $NAME_OF_THE_PROJECT
** File description:
** Here is coded a super function
*/

#include "lemin.h"

void add_last_node(room **list_addr, room *node)
{
    room *list = *list_addr;

    if (list == NULL) {
        list = node;
        return ;
    }
    while (list->next != NULL)
        list = list->next;
    list->next = node;
}
