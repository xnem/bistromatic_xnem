/*
** EPITECH PROJECT, 2018
** $NAME_OF_THE_PROJECT
** File description:
** Here is coded a super function
*/

#include "lemin.h"

void get_tunnel(room **list, char *line)
{
    size_t size = 0;
    room *first;
    room *second;

    my_putstr(line);
    first = search_room(*list, get_string_dash(&line));
    second = search_room(*list, get_string_dash(&line));
    if (first != NULL)
        add_last_node_linked(&first, room_dup(second));
}

void get_tunnels(room **list, char *last_line)
{
    char *line = last_line;
    size_t size = 0;

    my_putstr("#tunnels\n");
    get_tunnel(list, line);
    while (getline(&line, &size, stdin) != -1)
        get_tunnel(list, line);
}
