/*
** EPITECH PROJECT, 2018
** $NAME_OF_THE_PROJECT
** File description:
** Here is coded a super function
*/

#include "lemin.h"

void move_ant(int index, char *to_go, room **list, ant **tab_ant)
{
    room *room_to_go = search_room(*list, to_go);
    ant *tab_ants = *tab_ant;

    index--;
    tab_ants[index].room_in->nb_ant -= 1;
    tab_ants[index].room_in = room_to_go;
    room_to_go->nb_ant += 1;
}
