/*
** EPITECH PROJECT, 2018
** $NAME_OF_THE_PROJECT
** File description:
** Here is coded a super function
*/

#include "lemin.h"

int is_in_last_room(ant to_test, room *last)
{
    if (!my_strcmp(to_test.room_in->name, last->name))
        return (1);
    return (0);
}
