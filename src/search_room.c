/*
** EPITECH PROJECT, 2018
** $NAME_OF_THE_PROJECT
** File description:
** Here is coded a super function
*/

#include "lemin.h"

room *search_room(room *list, char *name)
{
    while (list != NULL) {
        if (!my_strcmp(list->name, name))
            return (list);
        list = list->next;
    }
    return (NULL);
}
