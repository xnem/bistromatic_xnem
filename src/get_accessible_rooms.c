/*
** EPITECH PROJECT, 2018
** $NAME_OF_THE_PROJECT
** File description:
** Here is coded a super function
*/

#include "lemin.h"

room *get_accessible_rooms(ant ant_t)
{
    room *room_t;

    if (ant_t.room_in->linked_rooms != NULL)
        return (room_t = ant_t.room_in->linked_rooms->adress);
    return (NULL);
}
