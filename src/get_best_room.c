/*
** EPITECH PROJECT, 2018
** $NAME_OF_THE_PROJECT
** File description:
** Here is coded a super function
*/

#include "lemin.h"

int get_best_room(ant ant_t, room *list)
{
    room *last = get_last_room(list);

    while (my_strcmp(last->predecessor->name, ant_t.room_in->name)) {
        last = last->predecessor;
    }
    printf("better room = %s\n", last->name);
    return (last);
}
