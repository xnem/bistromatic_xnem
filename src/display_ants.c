/*
** EPITECH PROJECT, 2018
** $NAME_OF_THE_PROJECT
** File description:
** Here is coded a super function
*/

#include "lemin.h"

void display_ants(ant *tab_ants)
{
    for (int i = 0; tab_ants[i].room_in != NULL; i++) {
        my_putstr("ant P");
        my_put_nbr(tab_ants[i].name);
        my_putstr(" in room ");
        my_putstr(tab_ants[i].room_in->name);
        my_putstr("\n");
    }
}
