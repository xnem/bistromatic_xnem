/*
** EPITECH PROJECT, 2018
** $NAME_OF_THE_PROJECT
** File description:
** Here is coded a super function
*/

#include "lemin.h"

void display_linked(room_linked *list)
{
    my_putstr("\nlinked rooms:\n");
    while (list != NULL) {
        my_putstr(list->name);
        list = list->next;
        if (list != NULL)
            my_putstr(" - ");
    }
    my_putstr("\n");
}

void display_list(room *list)
{
    my_putstr("\n");
    while (list != NULL) {
        my_putstr("name = ");
        my_putstr(list->name);
        my_putchar('\n');
        my_putstr("nb_ant = ");
        my_put_nbr(list->nb_ant);
        my_putchar('\n');
        my_putstr("length = ");
        my_put_nbr(list->length);
        my_putchar('\n');
        my_putstr("predecessor = ");
        if (list->predecessor != NULL)
            my_putstr(list->predecessor->name);
            my_putchar('\n');
        my_putstr("x = ");
        my_put_nbr(list->x);
        my_putchar('\n');
        my_putstr("y = ");
        my_put_nbr(list->y);
        display_linked(list->linked_rooms);
        list = list->next;
        if (list != NULL)
            my_putstr("---------------\n");
    }
    my_putchar('\n');
}
