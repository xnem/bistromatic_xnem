/*
** EPITECH PROJECT, 2018
** $NAME_OF_THE_PROJECT
** File description:
** Here is coded a super function
*/

#include "lemin.h"

room *create_empty_node(void)
{
    room *new_node = NULL;

    new_node = malloc(sizeof(room));
    new_node->next = NULL;
    new_node->linked_rooms = NULL;
    if (new_node == NULL)
        exit (84);
    return (new_node);
}
