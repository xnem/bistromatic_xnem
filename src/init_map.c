/*
** EPITECH PROJECT, 2018
** $NAME_OF_THE_PROJECT
** File description:
** Here is coded a super function
*/

#include "lemin.h"

void init_map(room *list)
{
    while (list != NULL) {
        list->predecessor = NULL;
        list->length = 0;
        list->visited = 0;
        list = list->next;
    }
}
