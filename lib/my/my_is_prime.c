/*
** EPITECH PROJECT, 2018
** $NAME_OF_THE_PROJECT
** File description:
** Here is coded a super function
*/

int my_is_prime(int nb)
{
    if (nb == 2)
        return (1);
    if (nb < 2 || !(nb % 2))
        return (0);
    for (int i = 3; i < nb; i += 2) {
        if (nb % i == 0)
            return (0);
    }
    return (1);
}
