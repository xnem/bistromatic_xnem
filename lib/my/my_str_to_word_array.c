/*
** EPITECH PROJECT, 2018
** $NAME_OF_THE_PROJECT
** File description:
** Here is coded a super function
*/

#include <stdlib.h>

int my_strlen(char const *str);

char *my_strcpy(char *dest, char const *src);

int my_char_isalphaornum(char const str)
{
    if (!((str >= 'a' && str <= 'z') || (str >= 'A' && str <= 'Z')) &&
            !(str >= '0' && str <= '9'))
        return (0);
    return (1);
}

int nb_words_function(char *str)
{
    int word_nb = 0;

    for (int i = 0; str[i] != '\0'; i++) {
        while (!my_char_isalphaornum(str[i]) && str[i] != '\0')
            i++;
        if (str[i] != '\0')
            word_nb++;
        while (my_char_isalphaornum(str[i]) && str[i] != '\0')
            i++;
    }
    return (word_nb);
}

char **create_tab(char *str)
{
    int word_size;
    int nb_word = nb_words_function(str);
    char **tab_p = malloc(sizeof(char *) * (nb_word + 1));

    for (int i = 0, j = 0; j < nb_words_function(str); j++) {
        word_size = 0;
        while (!my_char_isalphaornum(str[i]) && str[i] != '\0')
            i++;
        while (my_char_isalphaornum(str[i]) && str[i] != '\0') {
            i++;
            word_size++;
        }
        tab_p[j] = malloc(sizeof(char) * (word_size + 1));
        tab_p[j][word_size] = '\0';
    }
    tab_p[nb_word] = 0;
    return (tab_p);
}

char **my_str_to_word_array(char const *str)
{
    char *temp_str = malloc(sizeof(char) * (my_strlen(str) + 1));
    char **tab;
    int t_counter;

    my_strcpy(temp_str, str);
    tab = create_tab(temp_str);
    for (int i = 0, j = 0; j < nb_words_function(temp_str); j++) {
        t_counter = 0;
        while (!my_char_isalphaornum(str[i]) && str[i] != '\0')
            i++;
        while (my_char_isalphaornum(str[i]) && str[i] != '\0') {
            tab[j][t_counter] = str[i];
            t_counter++;
            i++;
        }
    }
    return (tab);
}
