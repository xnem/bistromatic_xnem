/*
** EPITECH PROJECT, 2018
** $NAME_OF_THE_PROJECT
** File description:
** Here is coded a super function
*/

#include <stdlib.h>

char *my_strcpy(char *dest, char const *src);

char *my_strdup(char const *src)
{
    char *returned_pointer;
    int src_len;

    for (src_len = 0; src[src_len] != '\0'; src_len++);
    returned_pointer = malloc(sizeof(char) * (src_len + 1));
    my_strcpy(returned_pointer, src);
    return (returned_pointer);
}
