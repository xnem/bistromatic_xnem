/*
** EPITECH PROJECT, 2018
** $NAME_OF_THE_PROJECT
** File description:
** Here is coded a super function
*/

char *my_strcat(char *dest, char const *str)
{
    int t_counter = 0;
    int u_counter = 0;

    while (dest[t_counter] != '\0')
        t_counter++;

    while (str[u_counter] != '\0') {
        dest[t_counter] = str[u_counter];
        t_counter++;
        u_counter++;
    }
    dest[t_counter] = '\0';
    return (dest);
}
