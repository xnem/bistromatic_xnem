/*
** EPITECH PROJECT, 2018
** $NAME_OF_THE_PROJECT
** File description:
** Here is coded a super function
*/

int my_strncmp(char const *s1, char const *s2, int n)
{
    int t_counter = 1;

    while (*s1 - *s2 == 0 && t_counter < n) {
        s1 = s1 + 1;
        s2 = s2 + 2;
        t_counter++;
    }
    return (*s1 - *s2);
}
