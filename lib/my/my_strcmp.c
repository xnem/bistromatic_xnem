/*
** EPITECH PROJECT, 2018
** $NAME_OF_THE_PROJECT
** File description:
** Here is coded a super function
*/

#include <stdio.h>
#include <string.h>

int my_strcmp(char const *s1, char const *s2)
{
    while (*s1 - *s2 == 0 && *s1 != '\0') {
        s1 = s1 + 1;
        s2 = s2 + 1;
    }
    return (*s1 - *s2);
}
