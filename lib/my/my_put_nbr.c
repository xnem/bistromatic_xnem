/*
** EPITECH PROJECT, 2018
** my_put_nbr
** File description:
** my put nbr
*/

void my_putchar(char c);

int my_super_function(long nb)
{
    if (nb < 0) {
        my_putchar('-');
        my_super_function(nb * -1);
    }
    else if (nb < 10) {
        my_putchar(nb + '0');
    }
    else {
        my_super_function(nb / 10);
        my_putchar(nb % 10 + '0');
    }
    return (0);
}

int my_put_nbr(int nb)
{
    my_super_function((long)nb);
    return (0);
}
