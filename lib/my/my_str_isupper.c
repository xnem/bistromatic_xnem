/*
** EPITECH PROJECT, 2018
** $NAME_OF_THE_PROJECT
** File description:
** Here is coded a super function
*/

int my_str_isupper(char const *str)
{
    while (*str != '\0') {
        if (!(*str >= 'A' && *str <= 'Z'))
            return (0);
        str++;
    }
    return (1);
}
