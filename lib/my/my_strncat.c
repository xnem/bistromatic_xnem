/*
** EPITECH PROJECT, 2018
** $NAME_OF_THE_PROJECT
** File description:
** Here is coded a super function
*/

char *my_strncat(char *dest, char const *str, int nb)
{
    int t_counter = 0;
    int u_counter = 0;

    while (dest[t_counter] != '\0')
        t_counter++;
    while (u_counter < nb) {
        dest[t_counter] = str[u_counter];
        t_counter++;
        u_counter++;
    }
    dest[t_counter] = '\0';
    return (dest);
}
