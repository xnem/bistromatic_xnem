/*
** EPITECH PROJECT, 2018
** my evil str
** File description:
** Here is coded a super function
*/

int my_strlen(char const *str)
{
    int i = 0;

    while (str[i] != '\0')
        i++;
    return (i);
}

void my_swap(char *a, char *b)
{
    char v_swap;

    v_swap = *a;
    *a = *b;
    *b = v_swap;
}

char *my_revstr(char *str)
{
    int counter_up = 0;
    int counter_down = my_strlen(str) - 1;

    while (counter_up < counter_down) {
        my_swap(str + counter_up, str + counter_down);
        counter_up++;
        counter_down--;
    }
    return (str);
}
