/*
** EPITECH PROJECT, 2018
** my compute power rec
** File description:
** Here is coded a super function
*/

long this_is_the_function(long nb, int p)
{
    if (p == 0)
        return (1);
    if (p < 0)
        return (0);
    return (nb * this_is_the_function(nb, p - 1));
}

int my_compute_power_rec(int nb, int p)
{
    long t_result = this_is_the_function(nb, p);

    if (t_result < -2147483648 || t_result > 2147483647)
        return (0);
    return ((int)t_result);
}
