/*
** EPITECH PROJECT, 2018
** $NAME_OF_THE_PROJECT
** File description:
** Here is coded a super function
*/

int my_is_prime(int nb);

int my_find_prime_sup(int nb)
{
    int new_nb = nb;

    while (!(my_is_prime(new_nb)))
        new_nb++;
    return (new_nb);
}
