/*
** EPITECH PROJECT, 2018
** my get nbr
** File description:
** Here is coded a super function
*/

int my_superl_function(char const *str, int t_sign)
{
    long t_nbr = 0;

    while (*str <= '9' && *str >= '0') {
        t_nbr = t_nbr * 10 + *str - '0';
        str++;
    }
    if (t_nbr * t_sign < -2147483648 || t_nbr * t_sign > 2147483647)
        return (0);
    return (t_nbr * t_sign);
}

int my_getnbr(char const *str)
{
    int t_sign = 1;
    long t_number = 0;
    while (*str != '\0') {
        if (*str == '-' || *str == '+') {
            if (*str == '-')
                t_sign *= -1;
        }
        else if (*str >= '0' && *str <= '9')
            return (my_superl_function(str, t_sign));
        else
            return (0);
        str++;
    }
    return (0);
}
