/*
** EPITECH PROJECT, 2018
** my strrcpy
** File description:
** Here is coded a super function
*/

char *my_strcpy(char *dest, char const *src)
{
    int t_counter = 0;

    while (src[t_counter] != '\0') {
        dest[t_counter] = src[t_counter];
        t_counter++;
    }
    dest[t_counter] = '\0';
    return (dest);
}
