/*
** EPITECH PROJECT, 2018
** my_isneg
** File description:
** super description
*/

void my_putchar(char c);

int my_isneg(int n)
{
    char result_t;

    if (n < 0)
        my_putchar('N');
    else
        my_putchar('P');
    my_putchar('\n');
    return (0);
}
