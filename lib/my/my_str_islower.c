/*
** EPITECH PROJECT, 2018
** $NAME_OF_THE_PROJECT
** File description:
** Here is coded a super function
*/

int my_str_islower(char const *str)
{
    while (*str != '\0') {
        if (!(*str >= 'a' && *str <= 'z'))
            return (0);
        str++;
    }
    return (1);
}
