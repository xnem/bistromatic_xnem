/*
** EPITECH PROJECT, 2018
** $NAME_OF_THE_PROJECT
** File description:
** Here is coded a super function
*/

int my_strlen(char const *str)
{
    int char_number = 0;

    while (*str != '\0') {
        char_number++;
        str++;
    }
    return (char_number);
}
