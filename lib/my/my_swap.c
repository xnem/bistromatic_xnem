/*
** EPITECH PROJECT, 2018
** my swap
** File description:
** my swap
*/

void my_swap(int *a, int *b)
{
    int v_swap;

    v_swap = *a;
    *a = *b;
    *b = v_swap;
}
