/*
** EPITECH PROJECT, 2018
** compute square root
** File description:
** Here is coded a super function
*/

int my_compute_square_root(int nb)
{
    long t_result = 0;
    long t_counter = 0;

    if (nb < 0)
        return (0);
    while (t_result <= nb) {
        t_result = t_counter * t_counter;
        if (t_counter * t_counter == nb)
            return ((int)t_counter);
        t_counter++;
    }
    return (0);
}
