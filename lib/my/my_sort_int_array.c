/*
** EPITECH PROJECT, 2018
** my sort int array
** File description:
** Here is coded a super function
*/

void my_sort_int_array(int *array, int size)
{
    int var_t;

    for (int t_counter = 0; t_counter < size - 1; t_counter++) {
        if (array[t_counter] > array[t_counter + 1]) {
            var_t = array[t_counter];
            array[t_counter] = array[t_counter + 1];
            array[t_counter + 1] = var_t;
            t_counter = -1;
        }
    }
}
