/*
** EPITECH PROJECT, 2018
** $NAME_OF_THE_PROJECT
** File description:
** Here is coded a super function
*/

#include "../../include/linked_l.h"
#include "../../include/my.h"
#include <stdlib.h>

void display_list(linked_list_t *list)
{
    while (list != NULL) {
        my_putstr(list->value);
        list = list->next;
        my_putchar('\n');
    }
}
