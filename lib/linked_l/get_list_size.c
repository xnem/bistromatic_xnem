/*
** EPITECH PROJECT, 2018
** $NAME_OF_THE_PROJECT
** File description:
** Here is coded a super function
*/

#include "../../include/linked_l.h"
#include <stdlib.h>

int get_list_size(linked_list_t *list)
{
    int i;

    for (i = 0; list != NULL; i++)
        list = list->next;
    return (i);
}
