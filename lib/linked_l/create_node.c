/*
** EPITECH PROJECT, 2018
** $NAME_OF_THE_PROJECT
** File description:
** Here is coded a super function
*/

#include "../../include/linked_l.h"
#include <stdlib.h>

linked_list_t *create_node(char *value)
{
    linked_list_t *new_node = malloc(sizeof(linked_list_t));

    new_node->value = value;
    return (new_node);
}
