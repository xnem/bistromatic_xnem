/*
** EPITECH PROJECT, 2018
** $NAME_OF_THE_PROJECT
** File description:
** Here is coded a super function
*/

#include "../../include/linked_l.h"
#include "../../include/my.h"

linked_list_t *create_list(char **list)
{
    linked_list_t *list_t = create_node(list[0]);
    linked_list_t *actual_node = list_t;

    for (int i = 1; list[i]; i++) {
        actual_node->next = create_node(list[i]);
        actual_node = actual_node->next;
    }
    return (list_t);
}
