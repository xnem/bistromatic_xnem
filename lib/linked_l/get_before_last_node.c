/*
** EPITECH PROJECT, 2018
** $NAME_OF_THE_PROJECT
** File description:
** Here is coded a super function
*/

#include "../../include/linked_l.h"
#include <stdlib.h>

linked_list_t *get_before_last_node(linked_list_t *list)
{
    linked_list_t *actual_node = list;

    if (list == NULL || list->next == NULL)
        return (NULL);
    while (actual_node->next->next != NULL)
        actual_node = actual_node->next;
    return (actual_node);
}
