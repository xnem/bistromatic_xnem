/*
** EPITECH PROJECT, 2018
** $NAME_OF_THE_PROJECT
** File description:
** Here is coded a super function
*/

#include "../../include/linked_l.h"
#include "../../include/my.h"

linked_list_t *create_list_arg(int ac, char **av)
{
    linked_list_t *list = create_node(av[1]);
    linked_list_t *actual_node = list;

    for (int i = 2; i < ac; i++) {
        actual_node->next = create_node(av[i]);
        actual_node = actual_node->next;
    }
    return (list);
}
