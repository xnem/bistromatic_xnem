/*
** EPITECH PROJECT, 2018
** $NAME_OF_THE_PROJECT
** File description:
** Here is coded a super function
*/

#include "../../include/linked_l.h"
#include <stdlib.h>

linked_list_t *get_second_node(linked_list_t *list)
{
    if (list == NULL || list->next == NULL)
        return (NULL);
    return (list->next);
}
