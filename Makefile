##
## EPITECH PROJECT, 2018
## Makefile
## File description:
## bonus
##

NAME	=		lem_in

SRC_DIR	=		src
OBJ_DIR	=		obj

SRC		=		$(wildcard $(SRC_DIR)/*.c)	
OBJ		=		$(SRC:$(SRC_DIR)/%.c=$(OBJ_DIR)/%.o)
INC		=		-Iinclude

all:			$(NAME)

$(NAME):		$(OBJ)
				make -C lib/my/
				$(CC) -o $(NAME) $(OBJ) -Llib/my -lmy

$(OBJ_DIR)/%.o:	$(SRC_DIR)/%.c
				$(CC) $(INC) -c -o $@ $<

clean:
				rm -f $(OBJ)
				make clean -C lib/my

fclean:			clean
				rm -f $(NAME)
				make fclean -C lib/my

re:				fclean all
