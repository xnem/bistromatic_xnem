/*
** EPITECH PROJECT, 2018
** $NAME_OF_THE_PROJECT
** File description:
** Here is coded a super function
*/

#ifndef LEMIN_
#define LEMIN_

typedef struct room_t
{
    char *name;
    int nb_ant;
    int x;
    int y;
    int length;
    int visited;
    struct room_t *predecessor;
    struct room_linked_t *linked_rooms;
    struct room_t *next;
} room;

typedef struct room_linked_t
{
    char *name;
    struct room_t *adress;
    struct room_linked_t *next;
} room_linked;

typedef struct ant_t
{
    int name;
    room *room_in;
} ant;

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "my.h"

void add_last_node(room **list_addr, room *node);
void add_last_node_linked(room **list_addr, room_linked *node);
void algo(room *list_rooms, ant *tab_ant, room *last);
int check_letter(char *str, char c);
room *create_empty_node(void);
void *create_first_node(char *str, int nb_ant, room **list);
room *create_node(char *str);
void display_algo(ant ant_t, room *room_t);
void display_ants(ant *tab_ants);
int display_error(char *str_error, char *str);
void display_linked(room_linked *list);
void display_list(room *list);
room *get_accessible_rooms(ant ant_t);
room *get_last_room(room *list);
char *get_string(char **str1);
char *get_string_dash(char **str1);
void get_tunnel(room **list, char *line);
void get_tunnels(room **list, char *last_line);
ant *init_ants(room *list);
int is_in_last_room(ant to_test, room *last);
int main(int ac, char **av);
void move_ant(int index, char *to_go, room **list, ant **tab_ant);
void create_rooms(char *line, int nb_ants, room **last, room **list);
room *read_file(void);
room_linked *room_dup(room *node);
int room_is_usable(ant ant_t, room *room_t);
int search_ant(int ant_t, ant *tab_ants);
room *search_room(room *list, char *name);

#endif
